class CreateTrials < ActiveRecord::Migration
  def change
    create_table :trials do |t|
      t.string :name
      t.datetime :timestamp
      t.string :answer
      t.string :text

      t.timestamps null: false
    end
  end
end
