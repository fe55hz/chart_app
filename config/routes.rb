Rails.application.routes.draw do
  resources :trials
  get 'static_pages/warehouse'
  get 'static_pages/index'
  get 'trials/index'
  
  get 'static_pages/home'
  get 'static_pages/q1'
  get 'static_pages/q2'
  get 'static_pages/q3'
  get 'static_pages/q4'
  get 'static_pages/q5'
  get 'static_pages/q6'
  get 'static_pages/q7'
  get 'static_pages/q8'
  get 'static_pages/q9'
  get 'static_pages/result'

#  get 'static_pages/q2?:id' => 'static_pages#q2'
#  get 'static_pages/q3?:id' => 'static_pages#q3'
#  get 'static_pages/q4?:id' => 'static_pages#q4'
#  get 'static_pages/q5?:id' => 'static_pages#q5'
#  get 'static_pages/q6?:id' => 'static_pages#q6'
#  get 'static_pages/q7?:id' => 'static_pages#q7'
#  get 'static_pages/q8?:id' => 'static_pages#q8'
#  get 'static_pages/q9?:id' => 'static_pages#q9'
#  get 'static_pages/result?:id' => 'static_pages#result'

  resources :trials
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'static_pages#home'
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
