json.array!(@trials) do |trial|
  json.extract! trial, :id, :name, :timestamp, :answer, :text
  json.url trial_url(trial, format: :json)
end
